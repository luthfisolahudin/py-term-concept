from lib import prompt
from lib.command import process


def main():
    while True:
        process(prompt())


if __name__ == '__main__':
    main()
