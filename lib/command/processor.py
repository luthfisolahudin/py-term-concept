from .command import ReturnCode
from ..repository import find

def process(input: str) -> ReturnCode:
    command = find(input)

    if command is None:
        print('Command not found')
        return ReturnCode.FAILED

    return command.exec(input)
