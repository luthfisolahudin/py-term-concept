from abc import ABC, abstractclassmethod
from enum import Enum
from typing import Callable

class ReturnCode(Enum):
    SUCCESS = 0
    FAILED = 1

class Command(ABC):
    @abstractclassmethod
    def entry(self) -> str:
        raise NotImplementedError()

    @abstractclassmethod
    def exec(self, input: str = None) -> ReturnCode:
        raise NotImplementedError()

class SimpleCommand(Command):
    def __init__(self, command: str, callable: Callable[[], ReturnCode]) -> None:
        self.command = command
        self.callable = callable

    def entry(self) -> str:
        return self.command

    def exec(self, _: str = None) -> ReturnCode:
        return self.callable()
