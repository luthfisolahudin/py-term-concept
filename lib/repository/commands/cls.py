from ...command import Command, ReturnCode
import os

class CommandImplementer(Command):
    def entry(self) -> str:
        return 'cls'

    def exec(self, input: str = None) -> ReturnCode:
        os.system('cls' if os.name == 'nt' else 'clear')

        return ReturnCode.SUCCESS
