from ...command import Command, ReturnCode

class CommandImplementer(Command):
    def entry(self) -> str:
        return 'hello'

    def exec(self, input: str = None) -> ReturnCode:
        print('Hello!')

        return ReturnCode.SUCCESS
