from ...command import Command, ReturnCode

class CommandImplementer(Command):
    def entry(self) -> str:
        return 'exit'

    def exec(self, input: str = None) -> ReturnCode:
        print('See you!')
        exit()
