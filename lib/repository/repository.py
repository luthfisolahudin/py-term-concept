from ..command import Command
from .commands.cls import CommandImplementer as ClearCommand
from .commands.exit import CommandImplementer as ExitCommand
from .commands.hello import CommandImplementer as HelloCommand

commands = [
    ClearCommand(),
    ExitCommand(),
    HelloCommand(),
]

def find(entry: str) -> Command | None:
    return next(filter(lambda cmd: cmd.entry() == entry, commands), None)
