from os import getcwd
from pathlib import Path


def prompt() -> str:
    pwd = getcwd().replace(str(Path.home()), '~')

    return input(f"{pwd} > ")
